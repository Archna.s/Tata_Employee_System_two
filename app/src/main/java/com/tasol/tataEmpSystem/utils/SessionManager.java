package com.tasol.tataEmpSystem.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.models.LoginModel;

public class SessionManager {

    private SharedPreferences mPrefs;
    private String PREF_USER_INFO = "pref_user_info";
    private String PREF_AppConfig = "pref_appconfig";


    public SessionManager(Context mContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setUserInfo(LoginModel model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_USER_INFO, json);
        e.apply();
    }

    public LoginModel getUserInfo() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_USER_INFO, "");
        LoginModel model = gson.fromJson(json, LoginModel.class);
        return model;
    }

    public void deleteAllSharePrefs() {
        SharedPreferences.Editor e = mPrefs.edit();
        e.clear().commit();
    }

    public void setAppConfig(AppConfigModel model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_AppConfig, json);
        e.apply();
    }

    public AppConfigModel getAppConfig() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_AppConfig, "");
        AppConfigModel model = gson.fromJson(json, AppConfigModel.class);
        return model;
    }

}
