package com.tasol.tataEmpSystem.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;

public class Utils {

    public static Bitmap finalBitmap;
    public static Bitmap originalBitmap;

    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
