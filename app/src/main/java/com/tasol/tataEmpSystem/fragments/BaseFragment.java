package com.tasol.tataEmpSystem.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.tasol.tataEmpSystem.R;


public class BaseFragment extends Fragment {
    private ProgressDialog dialog;
    public void dispProg() {

    }

    public void initPrg() {

    }

    public void disrog() {

    }

    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public Fragment getCurrentFragment() {
        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.main_fragment);
        return currentFragment;

    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void openkeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public boolean isOpenkeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        return imm.isAcceptingText();
    }

    public void replaceFragment(Fragment fragment, boolean isAddToBackStack) {

        if (isAddToBackStack) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).addToBackStack(null).commit();
        } else {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).commit();

        }
    }

    public void initDialog(Context context) {
        dialog = new ProgressDialog(context);

        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
    }


    public void showDialog() {
        if (!dialog.isShowing()) {

            dialog.show();
            dialog.setContentView(R.layout.my_progress);

        }
    }

    public void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
