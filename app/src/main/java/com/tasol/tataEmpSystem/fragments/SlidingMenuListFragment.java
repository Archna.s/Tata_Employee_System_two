package com.tasol.tataEmpSystem.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.adapters.DrawerAdapter;
import com.tasol.tataEmpSystem.models.DrawerModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class SlidingMenuListFragment extends BaseFragment {

    ListView listView;
    ArrayList<DrawerModel> list = new ArrayList<>();
    DrawerAdapter adapter;
    public static CircleImageView imageView;
    EditText edValue;
    public static TextView txtname;
    public static Context context;
    LinearLayout llIcons;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sliding_menu_list, null);

        imageView = (CircleImageView) view.findViewById(R.id.imageView);
        listView = (ListView) view.findViewById(R.id.lstSlidingMenu);
        txtname = (TextView) view.findViewById(R.id.txtname);
        llIcons = (LinearLayout) view.findViewById(R.id.llIcons);

/*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount() - 1; i++) {
                    fm.popBackStack();
                }
                ((KairosDashboardActivity) getActivity()).slidingMenu.toggle();

                if (list.get(position).name.equalsIgnoreCase("Inventory Queue")) {

                    AllInventoryQueueListingFragment signal_fragment1 = new AllInventoryQueueListingFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Profile")) {

                    ProfileFragment signal_fragment1 = new ProfileFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Accept Inventory")) {
                    ((KairosDashboardActivity) getActivity()).openScaneDialog(AllInventoryQueueListingFragment.class);
                } else if (list.get(position).name.equalsIgnoreCase("Log out")) {
                    showlogoutDoalog();
                } else if (list.get(position).name.equalsIgnoreCase("Double checkout")) {
                    DoubleCheckoutlistFragment signal_fragment1 = new DoubleCheckoutlistFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Support")) {
                    ContactUsFragment signal_fragment1 = new ContactUsFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Used Inventory")) {
                    ((KairosDashboardActivity) getActivity()).openScaneDialog(OutBoundlistingFragment.class);
                */
/*    WereHouseActivity signal_fragment1 = new WereHouseActivity();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).addToBackStack(null).commit();
*//*

                } else if (list.get(position).name.equalsIgnoreCase("Find Location")) {
                    LocationFinderFragment signal_fragment1 = new LocationFinderFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else {
                    Toast.makeText(getActivity(), "Under Development.", Toast.LENGTH_SHORT).show();

                }

                // ((DashboardActivity) getActivity()).tooggelSlidingMenu();
            }
        });
*/

        setup();

        Log.e("333", "onCreateView");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }



    private void setup() {

        // imageView.setImageResource(R.mipmap.babay);

        DrawerModel model = new DrawerModel();
        model.name = "Dashboard";
        model.img = R.mipmap.ic_launcher;
        list.add(model);

        model = new DrawerModel();
        model.name = "Recognize";
        model.img = R.mipmap.ic_launcher;
        list.add(model);


        adapter = new DrawerAdapter(getActivity(), list);
        listView.setAdapter(adapter);

        llIcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://appsoft.us/"));
                context.startActivity(browserIntent);
            }
        });

    }


    /*  - User cant see price while modify order
     * - Need to changge aboutus url */


}
