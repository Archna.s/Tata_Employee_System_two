package com.tasol.tataEmpSystem.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.adapters.CustomArrayAdapter;
import com.tasol.tataEmpSystem.adapters.ScheduleAdapter;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class SheduleFragment extends BaseFragment {

    RecyclerView recylerview;
    HorizontalCalendar horizontalCalendar;
    Spinner spinner;
    boolean isFirstTime = true;
    ArrayList<String> monthlist;
    ArrayList<String> allmonthlist = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shedule, container, false);


        setup(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.GONE);
    }

    private void setupToolbar() {
        ((DashboardActivity) getActivity()).rightImageVisibility(View.GONE);
        ((DashboardActivity) getActivity()).setTitle("Schedule");
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.VISIBLE);
        ((DashboardActivity) getActivity()).llTitleOnClicklistener("dropdown");


    }

    private void setup(View view) {

        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        spinner = (Spinner) view.findViewById(R.id.spinner);

        Bundle args = getArguments();
        if (args != null) {

            monthlist = (ArrayList<String>) args.getSerializable("monthlist");
        }

        addAllmonthlist();
        setupRecylerview();
        setDates(view, true);
        setupDropDown();
    }

    private void addAllmonthlist() {

        allmonthlist.add("January");
        allmonthlist.add("February");
        allmonthlist.add("March");
        allmonthlist.add("April");
        allmonthlist.add("May");
        allmonthlist.add("June");
        allmonthlist.add("July");
        allmonthlist.add("August");
        allmonthlist.add("September");
        allmonthlist.add("October");
        allmonthlist.add("November");
        allmonthlist.add("December");
    }

    private void setDates(View view, boolean isCurrent) {
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();


        if (isCurrent) {
            int year = startDate.get(Calendar.YEAR);
            int month = startDate.get(Calendar.MONTH);
            int day = 1;
            startDate.set(year, month, day);

            LocalDate lastDayOfMonth = new LocalDate(year, month + 1, 1).dayOfMonth().withMaximumValue();
            int lastDay = Integer.parseInt(lastDayOfMonth.toString("dd"));
            String lastDay1 = (lastDayOfMonth.toString("MM/dd/yyyy"));
            Log.e("1111", "last day 123 = " + lastDay1);

            endDate.set(year, month, lastDay);


        } else {
        }
        setupCalander(view, startDate, endDate);

    }

    private void setupDropDown() {

        ArrayList<String> categories = new ArrayList<String>();
        categories.add("All");
        categories.add("April");
        categories.add("May");
        categories.add("June");
        categories.add("July");

      /*  ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
*/
        CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(),
                R.layout.row_spinner_month, monthlist);
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (!isFirstTime) {

                    Calendar startDate = Calendar.getInstance();
                    Calendar endDate = Calendar.getInstance();

                    int selectedMonth = 0;
                    for (int j = 0; l < allmonthlist.size(); j++) {
                        if (monthlist.get(i).contains(allmonthlist.get(j))) {
                            selectedMonth = j;
                            break;
                        }
                    }
                    Log.e("222", "selected month  : " + selectedMonth);

                    int year = startDate.get(Calendar.YEAR);
                    int month = selectedMonth;
                    int day = 1;

                    LocalDate lastDayOfMonth = new LocalDate(year, month + 1, 1).dayOfMonth().withMaximumValue();
                    int lastDay = Integer.parseInt(lastDayOfMonth.toString("dd"));
                    String lastDay1 = (lastDayOfMonth.toString("MM/dd"));
                    Log.e("1111", "last day  = " + lastDay1);

                    startDate.set(year, month, day);
                    endDate.set(year, month, lastDay);

                    //setupCalander(view, startDate, endDate);

                    horizontalCalendar.setRange(startDate, endDate);
                    horizontalCalendar.refresh();

                }
                isFirstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void performSpinnerClick() {

        spinner.performClick();


    }

    private void setupCalander(View view, Calendar startDate, Calendar endDate) {

       /* Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.FEBRUARY, -1);


        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.MARCH, 1);
*/
        final Calendar defaultSelectedDate = Calendar.getInstance();


        // int numOfDaysInMonth = startDate.getActualMaximum(Calendar.DAY_OF_MONTH);
        //   System.out.println("First Day of month: " + startDate.getTime());


        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                // .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .sizeTopText(0)
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffffff"))
                .end()
                // .defaultSelectedDate(defaultSelectedDate)
                /*.addEvents(new CalendarEventsPredicate() {

                    Random rnd = new Random();

                    @Override
                    public List<CalendarEvent> events(Calendar date) {
                        List<CalendarEvent> events = new ArrayList<>();
                        int count = rnd.nextInt(6);

                        for (int i = 0; i <= count; i++) {
                            events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                        }

                        return events;
                    }
                })*/
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Toast.makeText(getContext(), DateFormat.format("EEE, MMM d, yyyy", date) + " is selected!", Toast.LENGTH_SHORT).show();

                Calendar startDate = Calendar.getInstance();
                Calendar endDate = Calendar.getInstance();


                Log.e("AAA", DateFormat.format("dd", date) + "");
                Log.e("AAA", DateFormat.format("MM", date) + "");
                Log.e("AAA", DateFormat.format("yyyy", date) + "");

                int year = Integer.parseInt(DateFormat.format("yyyy", date) + "");
                int month = Integer.parseInt(DateFormat.format("MM", date) + "");
                int day = Integer.parseInt(DateFormat.format("dd", date) + "");
                startDate.set(year, month, day);
                endDate.add(month, 1);

                // setupCalander(view, startDate, endDate);


            }

        });

    }

    private void setupRecylerview() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        ScheduleAdapter adapter = new ScheduleAdapter(getActivity());
        recylerview.setAdapter(adapter);

    }


}
