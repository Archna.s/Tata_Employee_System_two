package com.tasol.tataEmpSystem;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.tasol.tataEmpSystem.fragments.HistoryFragment;
import com.tasol.tataEmpSystem.fragments.MonthAttendaceFragment;
import com.tasol.tataEmpSystem.fragments.MonthWiseAttendanceChildeFragment;
import com.tasol.tataEmpSystem.fragments.SheduleFragment;

public class DashboardActivity extends BaseActivity {

    //ActivityDashboardBinding mBinding;
    ImageView ivRight, ivDropDown;
    TextView txtTitle;
    LinearLayout llTitle;
    public SlidingMenu slidingMenu;
    ImageView imgLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);

        setup();

        replaceFragment(new HistoryFragment(), false);


    }

    private void setup() {

        ivRight = (ImageView) findViewById(R.id.ivRight);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        llTitle = (LinearLayout) findViewById(R.id.llTitle);
        ivDropDown = (ImageView) findViewById(R.id.ivDropDown);
        imgLeft = (ImageView) findViewById(R.id.imgLeft);

        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // replaceFragment(new SheduleFragment(), true);
                if (getCurrentFragment() instanceof MonthAttendaceFragment) {
                    ((MonthAttendaceFragment) getCurrentFragment()).replaceSchedule();
                }

            }
        });
        setupSlider2();
    }

    public void rightImageVisibility(int visible) {
        ivRight.setVisibility(visible);
    }

    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    public void isVisibleivDrop(int visible) {
        ivDropDown.setVisibility(visible);
    }

    public void llTitleOnClicklistener(String str) {

        llTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (str.equalsIgnoreCase("dropdown")) {
                    if (getCurrentFragment() instanceof SheduleFragment) {
                        ((SheduleFragment) getCurrentFragment()).performSpinnerClick();
                    }
                }
            }
        });

    }

    private void setupSlider2() {

        slidingMenu = new SlidingMenu(DashboardActivity.this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        // slidingMenu.setShadowDrawable(R.drawable.common_plus_signin_btn_text_light_pressed);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.0f);
        slidingMenu.attachToActivity(DashboardActivity.this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.sliding_menu);
        slidingMenu.setSlidingEnabled(false);


        slidingMenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {
            @Override
            public void onOpened() {
                Log.e("333", "slioder opne");
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }


            }
        });

        slidingMenu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            @Override
            public void onClose() {
            }
        });

        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingMenu.toggle();
            }
        });

    }


}
