package com.tasol.tataEmpSystem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tasol.tataEmpSystem.fragments.MonthWiseAttendanceChildeFragment;
import com.tasol.tataEmpSystem.models.InfoWindowData;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.widgets.CustomInfoWindowGoogleMap;

import org.json.JSONObject;

public class MapFullScreenActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_full_screen);
        setupGoogleMap();
    }

    private void setupGoogleMap() {
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map));

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        try {
            for (int i = 0; i < MonthWiseAttendanceChildeFragment.locationArray.length(); i++) {
                JSONObject jsonObject1 = MonthWiseAttendanceChildeFragment.locationArray.getJSONObject(i);
                String strLatitude = jsonObject1.getString("latitude");
                String strLongtitude = jsonObject1.getString("longitude");

//                double lat = Double.parseDouble(strLatitude);
//                double lng = Double.parseDouble(strLongtitude);
//                LatLng latLng = new LatLng(lat, lng);
//                googleMap.addMarker(new MarkerOptions().position(latLng)
//                        .title(jsonObject1.getString("punch_type")));
//                if (i == MonthWiseAttendanceChildeFragment.locationArray.length() - 1) {
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.0f));
//                }


                double lat = Double.parseDouble(strLatitude);
                double lng = Double.parseDouble(strLongtitude);
                LatLng latLng = new LatLng(lat, lng);
                MarkerOptions markerOptions = new MarkerOptions().position(latLng);

                InfoWindowData infoWindowData = new InfoWindowData();
                infoWindowData.setStrImage(ApiClient.BASE_URL + jsonObject1.getString("face_image"));
                String location = String.valueOf(jsonObject1.getInt("remote"));
                if (location.equalsIgnoreCase("1")) {
                    infoWindowData.setPunch_location("Location : Remote");
                } else {
                    infoWindowData.setPunch_location("Location : Office");
                }
                infoWindowData.setPunch_time("Time : " + jsonObject1.getString("checkdate") + " " + jsonObject1.getString("checkIntime"));
                infoWindowData.setPunch_type("Type : " + jsonObject1.getString("punch_type"));

                CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(MapFullScreenActivity.this);
                googleMap.setInfoWindowAdapter(customInfoWindow);

                Marker marker = googleMap.addMarker(markerOptions);
                marker.setTag(infoWindowData);
                marker.showInfoWindow();
                marker.hideInfoWindow();
                if (i == MonthWiseAttendanceChildeFragment.locationArray.length() - 1) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.0f));
                }

            }
        } catch (Exception e) {

        }

    }
}
