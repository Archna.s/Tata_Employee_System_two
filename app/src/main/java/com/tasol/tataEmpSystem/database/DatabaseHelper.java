package com.tasol.tataEmpSystem.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    // testing
    public static int count = 1;
    private static final int DATABASE_VERSION = 11;
    private static final String DATABASE_NAME = "tata_emp";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TakeUserPhotoHelper.CREATE_TABLE);


    }

    public void dropAllTables() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TakeUserPhotoHelper.TABLE_NAME + "");

    }

    public void deleteAllTables() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TakeUserPhotoHelper.TABLE_NAME + "");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //     sqLiteDatabase.execSQL(NotificationHelper.CREATE_TABLE);


        this.onCreate(sqLiteDatabase);
    }
}
