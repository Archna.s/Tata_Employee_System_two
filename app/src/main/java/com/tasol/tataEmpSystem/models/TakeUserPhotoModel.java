package com.tasol.tataEmpSystem.models;

public class TakeUserPhotoModel {

    public String id;
    public String latitude;
    public String longitude;
    public String imagepath;
    public String timestamp;
    public String address;
}
