package com.tasol.tataEmpSystem.models;

public class LoginModel {
    public String id;
    public String email;
    public String fullName;
    public String gender;
    public String parentOf;
    public String checkMobileNo;
    public String username;
    public String photo;
    public String role;
    public String istrain;
    public String thumb;
    public String added_on;

}
