package com.tasol.tataEmpSystem.adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tasol.tataEmpSystem.R;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class CustomArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<String> items;
    private final int mResource;

    public CustomArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull ArrayList objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txtMonthName = (TextView) view.findViewById(R.id.txtMonthName);
        View view1 = view.findViewById(R.id.view1);

        if (position == 0) {
            txtMonthName.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
        } else {
            txtMonthName.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
        }
        StringTokenizer tokens = new StringTokenizer(items.get(position), " ");
        String first_string = tokens.nextToken();
        txtMonthName.setText(first_string);

        return view;
    }
}
