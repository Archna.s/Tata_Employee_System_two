package com.tasol.tataEmpSystem.beaconutils;

import android.bluetooth.le.ScanResult;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by tasol on 14/6/18.
 */

public class ScanResultUtils {

    public static void getSorted(List<ScanResult> scanRes){
        Collections.sort(scanRes, new Comparator<ScanResult>() {
            @Override
            public int compare(ScanResult o1, ScanResult o2) {

                int txPower1 = (int) o1.getScanRecord().getBytes()[29];
                double rssi1 = ((double) o1.getRssi());
                double currDist1 = calculateDistance(txPower1, rssi1);

                int txPower2 = (int) o2.getScanRecord().getBytes()[29];
                double rssi2 = ((double) o2.getRssi());
                double currDist2 = calculateDistance(txPower2, rssi2);

                return ((Double)currDist2).compareTo((Double)currDist1);
            }
        });
    }

    public static List<ScanResult> getSortedResult(List<ScanResult> scanRes){
        getSorted(scanRes);
        return scanRes;
    }

    public static double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }
        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
    }
}
