package com.tasol.tataEmpSystem.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RobotothinTextView extends android.support.v7.widget.AppCompatTextView {

    public RobotothinTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public RobotothinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public RobotothinTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/roboto.thin.ttf");
        setTypeface(customFont);
    }
}