package com.tasol.tataEmpSystem.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RobotoboldTextView extends android.support.v7.widget.AppCompatTextView{

    public RobotoboldTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public RobotoboldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public RobotoboldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/roboto.bold.ttf");
        setTypeface(customFont);
    }
}